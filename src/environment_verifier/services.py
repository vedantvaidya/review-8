import os

from django.http import HttpResponse


class ResponseSatus:
    response = ""
    status = 200


def validate_environment_variables(environment_variables):
    ResponseSatus.response = ""
    ResponseSatus.status = 200

    for variable_name, conditions in environment_variables.items():
        environ_value = os.environ.get(variable_name)
        if environ_value is None:
            return HttpResponse(
                f"Error: {variable_name} is not present.\n",
                content_type="text/plain",
                status=400,
            )

        if (environ_value.isdigit() and ("." not in environ_value)) or (
            environ_value[0] == "-" and environ_value.lstrip("-").isdigit()
        ):
            environ_value = int(environ_value)

        environ_value_type = type(environ_value).__name__

        if conditions["type"] == "int":
            ResponseSatus.response = ResponseSatus.response + integer_validator(
                variable_name, environ_value, conditions, environ_value_type
            )

        if conditions["type"] == "string":
            ResponseSatus.response = ResponseSatus.response + string_validator(
                variable_name, environ_value, conditions, environ_value_type
            )

    return HttpResponse(
        ResponseSatus.response, content_type="text/plain", status=ResponseSatus.status
    )


def integer_validator(variable_name, environ_value, conditions, environ_value_type):
    if environ_value_type != "int":
        ResponseSatus.status = 400
        return f"Error: {variable_name} should be a positive integer.\n"

    if conditions["min"] is not None:
        if environ_value < conditions["min"]:
            ResponseSatus.status = 400
            return f"Error: {variable_name} should be greater than or equal to {conditions['min']}.\n"

    if conditions["max"] is not None:
        if environ_value > conditions["max"]:
            ResponseSatus.status = 400
            return f"Error: {variable_name} should be less than or equal to {conditions['max']}.\n"

    return f"Success: {variable_name} is Correct \n "


def string_validator(variable_name, environ_value, conditions, environ_value_type):
    if environ_value_type != "str":
        ResponseSatus.status = 400
        return f"Error: {variable_name} should be a string.\n"

    environ_value = environ_value.strip()

    if conditions["min"] is not None:
        if len(environ_value) < conditions["min"]:
            ResponseSatus.status = 400
            return f"Error: {variable_name} should be greater than or equal to {conditions['min']}.\n"

    if conditions["max"] is not None:
        if len(environ_value) > conditions["max"]:
            ResponseSatus.status = 400
            return f"Error: {variable_name} should be less than or equal to {conditions['max']}.\n"

    return f"Success: {variable_name} is Correct \n "
