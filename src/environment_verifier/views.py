from django.http import HttpResponse
from django.shortcuts import render
from django.conf import settings
import io
from django.views.decorators.csrf import csrf_exempt
from environment_verifier.services import validate_environment_variables

@csrf_exempt
def environment_verify_view(request):
    environment_variables = {
        "VERSION": {"type": "int", "min": 0, "max": 3},
        "Name": {"type": "string", "min": None, "max": None},
        "allowed_hosts": {"type": "string", "min": None, "max": None},
    }
    return (validate_environment_variables(environment_variables))
