from django.urls import path
from . import views

urlpatterns = [
    path("verify-environement-variables/",views.environment_verify_view,name="verify-environement-variables")
]
