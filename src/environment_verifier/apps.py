from django.apps import AppConfig


class EnvironmentVerifierConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'environment_verifier'
