from django.test import TestCase

from environment_verifier.services import validate_environment_variables


class MyModelTestCase(TestCase):

    def test_version_less_than_minimum(self):
        environment_variables = {
            "VERSION": {"type": "int", "min": 4, "max": 6},
            "Name": {"type": "string", "min": None, "max": None},
            "allowed_hosts": {"type": "string", "min": None, "max": None},
        }
        response = validate_environment_variables(environment_variables)
        self.assertEqual(response.status_code, 400)

    def test_version_more_than_maximum(self):
        environment_variables = {
            "VERSION": {"type": "int", "min": 0, "max": 2},
            "Name": {"type": "string", "min": None, "max": None},
            "allowed_hosts": {"type": "string", "min": None, "max": None},
        }
        response = validate_environment_variables(environment_variables)
        self.assertEqual(response.status_code, 400)

    def test_version_must_be_in_string(self):
        environment_variables = {
            "VERSION": {"type": "string", "min": 0, "max": 3},
            "Name": {"type": "string", "min": None, "max": None},
            "allowed_hosts": {"type": "string", "min": None, "max": None},
        }
        response = validate_environment_variables(environment_variables)
        self.assertEqual(response.status_code, 400)

    def test_name_length_less_than_minimum(self):
        environment_variables = {
            "VERSION": {"type": "int", "min": 0, "max": 3},
            "Name": {"type": "string", "min": 10, "max": None},
            "allowed_hosts": {"type": "string", "min": None, "max": None},
        }
        response = validate_environment_variables(environment_variables)
        self.assertEqual(response.status_code, 400)

    def test_name_length_more_than_maximum(self):
        environment_variables = {
            "VERSION": {"type": "int", "min": 0, "max": 3},
            "Name": {"type": "string", "min": None, "max": 5},
            "allowed_hosts": {"type": "string", "min": None, "max": None},
        }
        response = validate_environment_variables(environment_variables)
        self.assertEqual(response.status_code, 400)

    def test_name_must_be_integer(self):
        environment_variables = {
            "VERSION": {"type": "int", "min": 0, "max": 3},
            "Name": {"type": "int", "min": None, "max": 5},
            "allowed_hosts": {"type": "string", "min": None, "max": None},
        }
        response = validate_environment_variables(environment_variables)
        self.assertEqual(response.status_code, 400)

    def test_allowed_hosts_length_less_than_minimum(self):
        environment_variables = {
            "VERSION": {"type": "int", "min": 0, "max": 3},
            "Name": {"type": "string", "min": None, "max": None},
            "allowed_hosts": {"type": "string", "min": 20, "max": None},
        }
        response = validate_environment_variables(environment_variables)
        self.assertEqual(response.status_code, 400)

    def test_allowed_hosts_length_more_than_maximum(self):
        environment_variables = {
            "VERSION": {"type": "int", "min": 0, "max": 3},
            "Name": {"type": "string", "min": None, "max": 5},
            "allowed_hosts": {"type": "string", "min": None, "max": 5},
        }
        response = validate_environment_variables(environment_variables)
        self.assertEqual(response.status_code, 400)

    def test_name_must_be_integer(self):
        environment_variables = {
            "VERSION": {"type": "int", "min": 0, "max": 3},
            "Name": {"type": "int", "min": None, "max": 5},
            "allowed_hosts": {"type": "int", "min": None, "max": None},
        }
        response = validate_environment_variables(environment_variables)
        self.assertEqual(response.status_code, 400)

    def test_variable_not_present(self):
        environment_variables = {
            "Password": {"type": "int", "min": 0, "max": 3},
        }
        response = validate_environment_variables(environment_variables)
        self.assertEqual(response.status_code, 400)

    def test_name_must_be_integer(self):
        environment_variables = {
            "VERSION": {"type": "int", "min": 0, "max": 3},
            "Name": {"type": "string", "min": None, "max": None},
            "allowed_hosts": {"type": "string", "min": None, "max": None},
        }
        response = validate_environment_variables(environment_variables)
        self.assertEqual(response.status_code, 200)
