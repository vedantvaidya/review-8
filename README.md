# Review 8

## Introduction
The project validates if all the env variables present in the dict are present and validate if it adheres to the conditions given.



### Tech Stack
| Tools | Version | Reference| Guide |
| ---  | ---     | ---      | --- |
| Python  | 3.11     | https://www.python.org/downloads/release/python-3110 |
| Django  | 4.2.4     | https://www.djangoproject.com  |


### Getting Started

* **Prerequisite**:  Make sure to Install above mentioned tools on you machine and follow configuration guide.
* Git clone this repository to your local machine.
* Go to cloned directory and create **.env** at root, copy all content from **.example** and fill in values for all environment variables
* Create a virtual env with name **".venv"** Refer, [Virtualenv Guide](https://www.geeksforgeeks.org/python-virtual-environment/)
* Change your working directory to **backend** with following command: 

    ```cd src```
* Install required packages from "requirements.txt" with following command:
    
    ```pip install -r requirements.txt```

    
* Then you can run your project with following command: 
    
    ```python manage.py runserver```

